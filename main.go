package main

import (
	"github.com/labstack/echo"
	"net/http"
)

var (
	// ErrHttpGenericMessage that is returned in general case, details should be logged in such case
	ErrHttpGenericMessage = echo.NewHTTPError(http.StatusInternalServerError, "something went wrong, please try again later")
)

func main() {
	// Command line flag parsing
	//
	e := echo.New()

	registerRoutes(e)
}

func registerRoutes(Echo *e) {
	// GET /signin
	//    - ask if token is okay(verify password)
	//    - ask for user object
	//    - ask for scopes/permissoins
}
